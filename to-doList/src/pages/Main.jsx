import ToDoList from "../components/ToDoList"
import ToDoSearch from "../components/ToDoSearch"

const Main = ({todo, setTodo}) => {
    return (
        <div className="w-3/6 m-auto">
            <h1>TodoSearch</h1>
            <ToDoSearch/>
            <h1>TodoList</h1>
            <ToDoList todo={todo} setTodo={setTodo}/>
        </div>
    )
}

export default Main