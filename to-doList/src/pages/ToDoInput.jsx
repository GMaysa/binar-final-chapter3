import AddAndUpdate from "../components/AddAndUpdate"

const ToDoInput = ({todo, setTodo, idUp}) => (
    <div className="w-3/6 m-auto">
        <h1>TodoInput</h1>
        <AddAndUpdate todo={todo} setTodo={setTodo} idUp={idUp}/>
    </div>
)

export default ToDoInput