import { UilPen, UilTrashAlt } from "@iconscout/react-unicons";
import { useNavigate } from "react-router-dom";

const Cells = ({ setActive, active}) => {
  const navigate = useNavigate()

  const handleChange = (id) => {
    setActive(
      active.map((prevData) =>
        prevData.id === id
          ? { ...prevData, complete: !prevData.complete }
          : prevData
      )
    );
  };

  const handleDelete = (id) => {
    const index = active.findIndex((prevData) => prevData.id === id);
    if (index !== -1) {
      const newTodo = [...active];
      newTodo.splice(index, 1);
      setActive(newTodo);
    }
  }

  return (
    <div className="cells mt-12 mb-28">
      {active.map((data) => (
        <div
          key={data.id}
          className="cell flex justify-between border-solid border-[1.5px] border-stone-200 rounded-sm px-8 py-4 mb-3"
        >
          <div className="left flex space-x-2">
            <input
              type="checkbox"
              onChange={() => handleChange(data.id)}
              className="cursor-pointer"
              id="myCheck"
              defaultChecked={data.complete}
            />
            <p
              className={
                data.complete ? "text-stone-300 line-through" : "text-stone-600"
              }
              id="note"
            >
              {data.task}
            </p>
          </div>
          <div className="right flex space-x-2">
            <UilPen onClick={() => navigate('/todoinput', {state: {val: data.task, idUp: data.id}})} className="text-amber-200 hover:text-amber-500 duration-700 cursor-pointer" />
            <UilTrashAlt onClick={() => handleDelete(data.id)} className="text-red-200 hover:text-red-500 duration-700 cursor-pointer" />
          </div>
        </div>
      ))}
    </div>
  );
};

export default Cells;
