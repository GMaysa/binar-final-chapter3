import { UilFilePlusAlt } from '@iconscout/react-unicons'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'

const AddAndUpdate = ({todo, setTodo}) => {

    const navigate = useNavigate()
    const location = useLocation()
    const val = location.state && location.state.val
    const idUp = location.state && location.state.idUp
    const [task, setTask] = useState(val || '')

    const handleSubmit = () => {
        console.log(idUp)
        if(idUp == null){
            const NewTask = {
                id: todo.length+1,
                task: task,
                complete: false
            }
            setTodo([...todo, NewTask])
        }else{
            const todoIndex = todo.findIndex(todo => todo.id == idUp)
            const upTask = {...todo[todoIndex], task: task}
            const updateTodo = [
                ...todo.slice(0, todoIndex),
                upTask,
                ...todo.slice(todoIndex+1)
            ]
            setTodo(updateTodo)
            // setTodo( todo.id == idUp ?)
        }
        setTask('')
        navigate('/')
    }

    

    return(
        <div className='p-4 flex justify-between border-solid border-[1.5px] border-stone-300 rounded-sm'>
            <div action="" className='h-auto w-full flex-col'>
                <div className="flex h-8 w-auto">
                    <UilFilePlusAlt className='font-bold h-auto w-auto p-2 rounded-l-sm text-white bg-cyan-600'/>
                    <input type="text" value={task} onChange={(e) => setTask(e.target.value)} className='w-full outline-none text-sm pl-2 border-y-[1.5px] border-r-[1.5px] border-stone-300 rounded-sm' placeholder='Search Todo...'/>
                </div>
                {task == '' ? 
                <button onClick={() => handleSubmit()} className="w-full text-sm font-center bg-cyan-600 opacity-60 text-white font-light py-2 mt-4 rounded-sm" disabled>Submit</button>
                :
                <button onClick={() => handleSubmit()} className="w-full text-sm font-center bg-cyan-600 text-white font-light py-2 mt-4 rounded-sm">Submit</button>
                }
            </div>
        </div>
    )
}

export default AddAndUpdate