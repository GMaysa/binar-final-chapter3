import { useEffect, useState } from 'react'
import Cells from './Cells'

const ToDoList = ({todo, setTodo}) => {
    const [active, setActive] = useState(todo)
    const handleFilter = (index) => {
        if (index == 0){
            setActive(todo)
        } else if (index == 1){
            setActive(todo.filter(todo => todo.complete == true))
        } else if (index == 2){
            setActive(todo.filter(todo => todo.complete == false))
        }
    }

    const handleDelete = (index) => {
        if(index == 0) {
            setTodo(todo.filter(todo => todo.complete == false))
            setActive(todo.filter(todo => todo.complete == false))
        }else if(index == 1){
            setTodo([])
            setActive([])
        } 
    }

    return(
        <div>
            <div className="list-button flex justify-between space-x-10">
                <button onClick={() => handleFilter(0)} id={active == 0 ? 'active' : ''}>All</button>
                <button onClick={() => handleFilter(1)} id={active == 1 ? 'active' : ''}>Done</button>
                <button onClick={() => handleFilter(2)} id={active == 2 ? 'active' : ''}>Todo</button>
            </div>
            
            <Cells setActive={setActive} active={active} />

            <div className="delete-buttons flex justify-between space-x-10 mt-8 fixed bottom-0 w-3/6 py-4 bg-white">
                <button onClick={() => handleDelete(0)}>Delete done tasks</button>
                <button onClick={() => handleDelete(1)}>Delete all tasks</button>
            </div>
        </div>
    )
}

export default ToDoList