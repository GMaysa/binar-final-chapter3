import { UilSearch, UilPlus} from '@iconscout/react-unicons'
import { Link } from 'react-router-dom'

const ToDoSearch = () => {
    return (
        <div className='p-4 flex justify-between border-solid border-[1.5px] border-stone-300 rounded-sm'>
            <form action="" className='h-auto w-3/5 flex-col'>
                <div className="flex h-8 w-auto">
                    <UilSearch className='font-bold h-auto w-auto p-2 rounded-l-sm text-white bg-cyan-600'/>
                    <input type="text" className='w-full outline-none text-sm pl-2 border-y-[1.5px] border-r-[1.5px] border-stone-300 rounded-sm' placeholder='Search Todo...'/>
                </div>
                <button className="w-full text-sm font-center bg-cyan-600 text-white font-light py-2 mt-4 rounded-sm">Search</button>
            </form>
            <Link to='/todoinput' className="flex flex-col items-center justify-center w-2/5 ml-8 p-2 text-sm text-center text-cyan-600 bg-cyan-50 border-dashed border-2 border-cyan-600 rounded-md">Add New Task <UilPlus className="h-4 w-4" /></Link>
        </div>
    )
}

export default ToDoSearch