import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { useState, useEffect } from "react";
import Main from "./pages/Main";
import ToDoInput from "./pages/ToDoInput";
import data from './components/data/data.json'

function App() {
  const [todo, setTodo] = useState(data)

  useEffect(() => {
      console.log(todo)
  }, [todo])

  return (
    <Router>
      <Routes>
        <Route index element={<Main todo={todo} setTodo={setTodo}/>}/>
        <Route path="/todoinput" element={<ToDoInput todo={todo} setTodo={setTodo}/>}/>
      </Routes>
    </Router>
  );
}

export default App;
